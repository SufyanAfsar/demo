<?php

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function record_count() {
        return $this->db->count_all('user');
    }

    function get_all_users_data($limit, $start) {
        $this->db->limit($limit, $start);
        $this->db->ORDER_BY('user_id', 'DESC');
        $query = $this->db->get('user');
        return $query;
    }

    function getAllRecords($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get('user');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
                //  echo "Model <pre>";print_r($row);exit;
            }
            return $data;
        }
        return false;
    }

    function login_authenticate($data) {
        //echo "Model <pre>";print_r($data);exit;
        $this->db->select('user_id,user_email,user_password,date_format');
        $this->db->from('user');
        $this->db->where('user_email', $data['user_email']);
        $this->db->where('user_password', md5($data['user_password']));
        $query = $this->db->get();
        //echo "Model <pre>";print_r($query);
        $resoonse['num_rows'] = 0;
        $resoonse['data'] = NULL;
        if ($query->result_id->num_rows > 0) {
            $resoonse['num_rows'] = $query->result_id->num_rows;
            $resoonse['data'] = $query->row();
//          $response = $query->row()->date_format;
        }
        return $resoonse;
    }

    function last_login_history($user_id, $limit, $start) {
        $this->db->select('id,tbl_user_id,login_time');
        $this->db->from('tbl_user_login_history');
        $this->db->where('tbl_user_id', $user_id);
        $this->db->ORDER_BY('id', 'DESC');
        $this->db->limit($start, $limit);
        $query = $this->db->get();
        if ($query->result_id->num_rows > 0) {
            $response = $query->row()->login_time;
        }
      //  echo $this->db->last_query();
      //  echo 'database time'.$response;
        return $response;
    }

    function insert_user_login_history($data) {
//        echo "  model LOGIN<pre>";
//        print_r($data);
        $user_login_data = array(
            'tbl_user_id' => $data['user_id'],
            'ip_address' => $data['Ip_address'],
            'user_agent' => $data['browser_version'],
            'user_os' => $data['User_os'],
            'login_time' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('tbl_user_login_history', $user_login_data);

        $current_login_id = $this->db->insert_id();
        return $current_login_id;
//        echo "login ID<pre>";
//        print_r($current_login_id);
//                exit;
    }

    function insert_user_logout_time($data) {
//         echo "<pre>";print_r($data);
//        exit;
        $user_login_id = $data['login_id']['user_loggedin_id'];
        $user_logout_time = array('logout_time' => date('Y-m-d H:i:s'));
        $this->db->where('id', $user_login_id);
        $query = $this->db->update('tbl_user_login_history', $user_logout_time);
    }

    function create_member($data) {
//        echo "before model<pre>";
//        print_r($data);
        $new_member_data = array(
            'user_fname' => $data['fname'],
            'user_lname' => $data['lname'],
            'user_email' => $data['user_email'],
            'user_password' => md5($data['password']),
            'user_dob' => date('Y-m-d'),
            'date_format' => $data['value']
        );
        return $this->db->insert('user', $new_member_data);
        //echo "===== here in create_member else consider";exit;
    }

    function check_email_exist($email) {
        $this->db->where('email', $email);
        $result->db->get('email');
        if ($result->num_rows() > 0) {
            // this will bring the value greater then 0 which shows that email exist 
            return $result;
        } else {
            // the email can be registered 
            return false;
        }
    }

    function remove_row($id) {
        //  echo "<pre>";print_r($this->input->post());exit;
        $this->db->where('user_id', $id);
        $this->db->delete('user');
    }

    function update_record($id) {
        $this->db->select('user.*');
        $this->db->from('user');
        $this->db->where("user_id=$id");
        $data = $this->db->get();
        $row = $data->row();
        return $row;
    }

    function update_member($data) {
        //   echo "<pre>";print_r($data);exit;
        $user_dob = $data['dob'];
        $explode_date = explode("/", $user_dob);
        $user_adjust_dob = $explode_date[0] . '-' . $explode_date[1] . '-' . $explode_date[2];
        $new_member_data = array(
            'user_fname' => $data['fname'],
            'user_lname' => $data['lname'],
            'user_email' => $data['user_email'],
            'user_password' => md5($data['password']),
            'user_dob' => $user_adjust_dob
        );
        $this->db->where('user_id', $data['user_id']);
        $query = $this->db->update('user', $new_member_data);
        return 1;
        //echo "===== here in create_member else consider";exit;
    }

    //==================for date insertion 
    function create_date($data) {
        $userdate = $data['date'];
        $dateMonth['datepicker_date'] = date('Y-m-d', strtotime($userdate));
        $query = $this->db->insert('datepicker', $dateMonth);
        echo "Modle<pre>";
        print_r($query);
        exit;
    }

    function create_date_format($data) {
        //echo "<pre>";print_r($data);exit;
        $userdate = $data['date'];
        $date_type = $data['date_type'];
        echo "Before === " . $userdate;
        if ($date_type == 1 || $date_type == 2) {
            $userdata1 = str_replace(',', ' ', $userdate);
        } else if ($date_type == 3) {
            $userdata1 = str_replace('-', ' ', $userdate);
        } else if ($date_type == 4) {
            $userdata1 = str_replace('/', '-', $userdate);
        }
        echo "<br>After  === " . $userdata1;
        $dateMonth['datepicker_date'] = date('Y-m-d', strtotime($userdata1));
        $query = $this->db->insert('datepicker', $dateMonth);
        echo "Modle<pre>";
        print_r($query);
        exit;
    }

    function getdate() {
        $this->db->select('datepicker.*');
        $this->db->from('datepicker');
        $query = $this->db->get();
        return $query->result();
    }

}
