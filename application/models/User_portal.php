<?php

    class User_portal extends CI_Model{
        
        function __construct() {
        parent::__construct();
    }
    function create_teacher_member($data){
        $new_member_data = array(
            'tech_name'        => $data ['tech_name'],
            'tech_designation' => $data ['tech_designation']);
            return $this->db->insert('teacher',$new_member_data);
    }   
    function get_teacher_data($limit,$start){
        $this->db->limit($limit, $start);
        $query = $this->db->get('teacher');
        // =====for dropdown as well

            if($query->num_rows() > 0){
            foreach ($query->result() as $row){            
                $data[]=$row;
        //echo "Model <pre>";print_r($row);exit;
            }
            return $data;
            }
        return false;
    }  
      //  ==========class insetion========
    function create_class_member($data){
        $members_data= array(
           'class_level'   => $data['class_level'],
           'class_name' => $data['class_name'],
           'tech_id' => $data['teacher'],
           'sub_id' => $data['subject'],
        );
     //echo "Model <pre>";print_r($members_data);exit;
        return $this->db->insert('class',$members_data);  
    }
    //=============geting subject for class=====
    function get_subject(){
        $query= $this->db->get('subject');
        $result = $query->result();
        
        $sub_id = array('-Select Subject-');
        $sub_name = array('-Select Subject-');
            for($i=0; $i<count ($result); $i++){
                array_push($sub_id,$result[$i]->sub_id);
                array_push($sub_name,$result[$i]->sub_name);
        }
        //echo "Model <pre>";print_r(array_combine($sub_id,$sub_name));exit;
        $newarray= array_combine($sub_id,$sub_name);
        return $newarray;  
    }
    //===========
     function get_teacher(){
        $query= $this->db->get('teacher');
        $result = $query->result();
        
        $tech_id = array('-Select teacher-');
        $tech_name = array('-Select teacher-');
            for($i=0; $i<count ($result); $i++){
                array_push($tech_id,$result[$i]->tech_id);
                array_push($tech_name,$result[$i]->tech_name);
        }
     //  echo "Model <pre>";print_r(array_combine($tech_id,$tech_name));exit;
        $newarray= array_combine($tech_id,$tech_name);
     //   echo "Modeldd <pre>";print_r($newarray);exit;
        return $newarray;  
    }
    //================Viewing data 
    function get_class_data($limit,$start){
        $this->db->limit($limit, $start);
        $query = $this->db->get('class');
            if($query->num_rows() > 0){
            foreach ($query->result() as $row){            
            $data[]=$row;
        //  echo "Model <pre>";print_r($row);exit;
            }
            return $data;
        }
        return false;
    }  
    //========for student
    function get_dropdown_list(){
        $query =$this->db->get('class');
             if ($query->num_rows() >= 1){
          // echo "model <pre>";print_r($query);exit;
             foreach ($query->result_array()as $row){
             $data[$row['class_id']]=$row['class_level'];
            }
      //  echo "model <pre>";print_r($data);exit;
        return $data;
        }
    }
    //==================teacher Subjects 
    
    
}
