-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2016 at 01:40 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_lname` varchar(20) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_dob` date NOT NULL,
  `user_fname` varchar(20) NOT NULL,
  `user_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_lname`, `user_password`, `user_dob`, `user_fname`, `user_email`) VALUES
(128, 'eeee', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'SDSDSDS', 'sajid@gmail.com'),
(129, 'ddddd', '3b712de48137572f3849aabd5666a4e3', '2016-12-23', 'dddd', 'sajid@gmail.com'),
(130, 'ssss', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(131, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(132, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(133, 'sdfasdfsaf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(134, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(135, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(136, 'eeee', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'SDSDSDS', 'sajid@gmail.com'),
(137, 'ddddd', '3b712de48137572f3849aabd5666a4e3', '2016-12-23', 'dddd', 'sajid@gmail.com'),
(138, 'ssss', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(139, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(140, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(141, 'sdfasdfsaf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(142, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(143, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(144, 'eeee', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'SDSDSDS', 'sajid@gmail.com'),
(145, 'ddddd', '3b712de48137572f3849aabd5666a4e3', '2016-12-23', 'dddd', 'sajid@gmail.com'),
(146, 'ssss', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(147, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(148, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(149, 'sdfasdfsaf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(150, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(151, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(152, 'eeee', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'SDSDSDS', 'sajid@gmail.com'),
(153, 'ddddd', '3b712de48137572f3849aabd5666a4e3', '2016-12-23', 'dddd', 'sajid@gmail.com'),
(154, 'ssss', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(155, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(156, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(157, 'sdfasdfsaf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(158, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(159, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
