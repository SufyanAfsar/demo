<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'user/login_view';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE; 
$route['signup']='User/user_signup';
$route['signup-submit']='User/create_user';
$route['index/(:any)']='User/user_data/$1';
$route['login-submit']='User/login_submit';
$route['update-submit']='User/update_user';
$route['logout']='User/logout';
$route['delete/(:any)']='User/delete_row/$1';
$route['update/(:any)']='User/edit_row/$1';
$route['users/(:any)']='User/users_data/$1';
//===========user_selected_date_format======
$route['createdate']='User/user_selected_date_format';

//===========datepicker===========

$route['datepicker']='User/datepicker';
$route['createdate']='User/createdate';
$route['datepickertwo']='User/datepickertwo';
$route['createdate']='User/createdateformat';
//==============view date=========
$route['viewdate']='User/viewdate';
$route['calendar']='User/user_selected_date_format';
//==========portal===========//

$route['teacher']='Portal/teacher';
$route['teacher-submit']='Portal/create_teacher';
$route['teacher-info/(:any)']='Portal/teacher_data/$1';
$route['class']='Portal/Section_class';
$route['class-submit']='Portal/create_class';
$route['class-info/(:any)']='Portal/class_data/$1';

$route['student']='Portal/student';
$route['subject']='Portal/subject';
$route['lesson']='User/lesson';

$route['history']='User/user_last_login_history';

