<?php
//===helps to call public Function of ***User*** controller in **Portal** Controller===// 
include_once (dirname(__FILE__) . "/User.php");


class portal extends User{
    function __construct() {
        parent::__construct();
           $this->load->model("User_portal");
           $this ->load->model("User_model");
        //  $this->load->controller("User");
    }
      public function teacher(){
           $this->check_authentication();

           $this->load->view('portal/teacher');
           //============ loading footer files a wau to include
          // $this->load->view('includes/footer');
        }
     function create_teacher(){
          $this->check_authentication();
         //echo "<pre>";print_r($this->input->post());exit;
          $response = 0;
             if($this->input->server("REQUEST_METHOD")=="POST"){
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                //Validations for data
                $this->form_validation->set_rules('tech_name','Teacher Name','required|trim');
                $this->form_validation->set_rules('tech_designation','Teacher Designation','required|trim');
             if ($this->form_validation->run()===false){
                 $response = 0;
             }else {
                 $db_response=$this->User_portal->create_teacher_member($this->input->post());
                 if($db_response){
                     $response = 1;
                 }else {
                     $response = 0;
                 }
             }
         }
            echo $response;
     } 
     
    //=====viewing teacher data===========//
     
     function teacher_data(){
            $this->check_authentication();
            $this->load->library("pagination");
         //loading library files for specific View 
             $config['base_url']= 'http://localhost/demo/teacher-info/';
             $config["per_page"] =3;
             $config["num_links"]=2;
             $config['first_url'] = $this->config->base_url().'teacher-info/0';
             $config["uri_segment"] = 2;
             $config["total_rows"]=$this->User_model->record_count() ;

             $this->pagination->initialize($config) ;
              
             $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
             
             $data["results"] = $this->User_portal->get_teacher_data($config["per_page"], $page);
         //    echo '<pre>';print_r($data);exit;
             $data["links"]   = $this->pagination->create_links() ;
             //echo print_r($this->input->post());exit;
             $this->load->view ("portal/teacherInformation",$data);
     }
//===============Classs===========//
     function Section_class(){
         $this->check_authentication();
         $data['subject']      = $this->User_portal->get_subject();
         $data['teacher'] = $this->User_portal->get_teacher();
//echo "Controler<pre>".$data;exit;

         $this->load->view('/portal/class',$data);
     }
     function create_class(){
         $this->check_authentication();
               $response = 0;
            if($this->input->server("REQUEST_METHOD")=="POST"){
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                //Validations for data
                $this->form_validation->set_rules('class_name','Class Name','required|trim');
                $this->form_validation->set_rules('class_level','Class Section','required|trim');
                $this->form_validation->set_rules('teacher','teacher','required|trim');
                $this->form_validation->set_rules('subject','subject','required|trim');

 // echo "cont<pre>"; print_r($this->input->post()); exit;
             if ($this->form_validation->run()===false){
                 $response = 0;
           //   echo "response".$response; exit;
             }else {
                 $db_response=$this->User_portal->create_class_member($this->input->post());
       //  echo 'con<pre>';print_r($db_response);
                              
                 if($db_response){
                     $response = 1;
                 }else {
                     $response = 0;
                 }
             }
         }
            echo $response;
     } 

     //=========viewing data
      function class_data(){
            $this->check_authentication();
            $this->load->library("pagination");
         //loading library files for specific View 
             $config['base_url']= 'http://localhost/demo/class-info/';
             $config["per_page"] =10;
             $config["num_links"]=3;
             $config['first_url'] = $this->config->base_url().'class-info/0';
             $config["uri_segment"] = 2;
             $config["total_rows"]=$this->User_model->record_count() ;
             $this->pagination->initialize($config) ;
             $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
             $data["results"] = $this->User_portal->get_class_data($config["per_page"], $page);
             $data["links"]   = $this->pagination->create_links() ;
             //echo print_r($this->input->post());exit;
             $this->load->view ("portal/classInformation",$data);
     }
 //   =============student
     function student (){
         $this->check_authentication();
         $data ['class']=$this->User_portal->get_dropdown_list();
         $this->load->view('portal/student',$data);
     }
     
 //    ================Suject
     function subject (){
         $this->check_authentication();
         $this->load->view('Portal/subject');
     }
}

