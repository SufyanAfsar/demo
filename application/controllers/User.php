
<?php

class User extends CI_Controller {

    function __construct() {
        parent ::__construct();
        $this->load->model("User_model");
    }

    public function check_authentication() {
        if (!$this->session->userdata('user_loggedin_id')) {
            redirect('/');
        }
    }

    // Submit login form
    function login_submit() {
        $response = 0;
        if ($this->input->server("REQUEST_METHOD") == "POST") {
            $this->form_validation->set_rules('user_email', 'Email', 'required|trim');
            $this->form_validation->set_rules('user_password', 'User Password', 'required|trim');
            if ($this->form_validation->run() == FALSE) {
                
            } else {
                $res = $this->User_model->login_authenticate($this->input->post());
                if ($res['num_rows'] > 0) {
                    $browserName = $this->agent->browser();
                    $browserVersion = $this->agent->version();
                    $browser_version_name = $browserName . " - " . $browserVersion;
                    $data = array(
//                      'user_email' => $res['data']->user_email,
                        'user_id' => $res['data']->user_id,
                        'Login_time' => Date('Y-m-d h:i:s'),
                        'Server_time ' => time('Y-m-d H:i:s'),
//                    'COUNTRY_CODE' =>$this->ip2location_lib->getCountryCode('8.8.8.8'),
//                    'LoginTime' =>$this->ip2location_lib->getCountryCode('8.8.8.8'),
                        'User_os' => $this->agent->platform(),
                        //'Ip_address' => $_SERVER['REMOTE_ADDR'],
                        'Ip_address' => $this->input->ip_address(),
                        'browser_version' => $browser_version_name,
                    );
                    //echo'ip adrress of this'.$this->input->ip_address();
                    //   echo "<pre>";print_r($res);
                    if (!empty($data)) {
                        $user_history = $this->User_model->insert_user_login_history($data);
                    }
                    //    echo "Response =";print_r($user_history);exit;
                    $this->session->set_userdata('user_id', $res['data']->user_id);
                    $this->session->set_userdata('user_loggedin_id', $user_history);
//                    echo "<preccccccccccc>";
//                    print_r($_SESSION);
//                    exit;
                    //echo "<pre>";print_r($_SESSION);exit;
//                    $data = array (
                    // $seesion_id = $_SESSION[__ci_last_regenerate],
//                         $user_id    = $_SESSION['user_loggedin_id'];
////                    );
//
                    $response = 1;
                } else {
                    $response = $res;
                }
                //echo "Response =".$response;exit;    
            }
        }
        echo $response;
    }

    function get_last_login_detail() {
        //getting the value from server and passing it to other functions
        //one is view date and the other one is user_last_login_history
        $user_id = $_SESSION['user_id'];
        $config["limit"] = 1;
        $config["offset"] = 1;
        $datetime = $this->User_model->last_login_history($user_id, $config["limit"], $config["offset"]);

        $lastlogindate['server_default_time'] = date_default_timezone_get();
        //echo'defualttime zone'.date_default_timezone_get(); exit;
        $asia_timestamp = strtotime($datetime);
        // echo '_______controller time '.$datetime;
        date_default_timezone_set('Asia/Karachi');
        $utcDateTimeZone = 'Asia/Karachi';
        $utcDateTime = date("h:i:A d-M-Y", $asia_timestamp);
        // echo '________afetr converting it'.$utcDateTime; 
        //$ip=$_SERVER['HTTP_X_FORWARDED_FOR']; 
        //  echo'Time adrress of this'. $this->lang->line('UM12');
        //$long = ip2long($ip);
        // echo'ip adrress of this' . $long;

        $lastlogindate['last_login_date'] = $utcDateTime;
        $lastlogindate['last_login_date_zone'] = $utcDateTimeZone;
        return $lastlogindate;
    }

    function viewdate() {
        $data = NULL;
        $datetime = $this->get_last_login_detail();
        $data['datetime'] = $datetime;
        //echo "<pre>";print_r($datetime);
        $query = $this->User_model->getdate();
        $data['date'] = null;
        if ($query) {
            $data['date'] = $query;
        }
//        echo "<pre>";
//        print_r($data);
//        exit;
        $this->load->view('viewdate', $data);
    }

    function user_last_login_history() {
        $datetime = $this->get_last_login_detail();
        //echo "<pre>";print_r($datetime);exit;      
        $this->load->view('history', $datetime);
    }

    function update_user() {
        $response = 0;
        if ($this->input->server("REQUEST_METHOD") == "POST") {
            //echo "<pre>";print_r($this->input->post());exit;
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('fname', 'First Name', 'required|trim');
            $this->form_validation->set_rules('lname', 'Last Name', 'required|trim');
            $this->form_validation->set_rules('user_email', 'Email', 'required|trim'/* |'call_back_check_email_exist' */);
            $this->form_validation->set_rules('password', 'Password', 'required|trim');
            $this->form_validation->set_rules('user_dob', 'DateOfBirth');
            if ($this->form_validation->run() === false) {
                $response = 0;
            } else {
                $db_response = $this->User_model->update_member($this->input->post());
                if ($db_response) {
                    $response = 1;
                } else {
                    $response = 0;
                }
            }
            echo $response;
        }
    }

    function create_user() {
        $response = 0;
        if ($this->input->server("REQUEST_METHOD") == "POST") {
            //echo "<pre>";print_r($this->input->post());exit;
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            // validation for Fisrt name
            $this->form_validation->set_rules('fname', 'First Name', 'required|trim');
            // validation for last name
            $this->form_validation->set_rules('lname', 'Last Name', 'required|trim');
            // validation for email
            // call_back is codeignator predefined so that our created finction can be called back 
            $this->form_validation->set_rules('user_email', 'Email', 'required|trim'/* |'call_back_check_email_exist' */);
            // validation for password
            $this->form_validation->set_rules('password', 'Password', 'required|trim');
            if ($this->form_validation->run() === false) {
                //  echo "===== here in valudation else";exit;
                $response = 0;
            } else {
                $db_response = $this->User_model->create_member($this->input->post());
                //echo "Db response=".$db_response;exit;
                if ($db_response) {
                    $response = 1;
                } else {
                    $response = 0;
                }
            }
        }
        //echo "Db response=".$response;exit;
        echo $response;
    }

    function edit_row($id) {
        $data = $this->User_model->update_record($id);
        //echo "In controller<pre>";print_r($data);exit;
        $this->load->view('update_form', $data);
        // echo "id".$id ;exit;
    }

    public function user_signup() {
        $this->load->view('signup');
    }

    // Login form view
    public function login_view() {
        $this->load->view('login_form');
    }

    public function logout() {
        $data = array(
            'login_id' => $_SESSION,
            'Logout_time' => date('Y-m-d H:i:s')
        );
//               echo "controller<pre>";print_r($data); 
        $user_history = $this->User_model->insert_user_logout_time($data);
        $this->session->sess_destroy();
        redirect("/");
    }

    function delete_row($id) {
        //echo "ID=".$id;exit;
        $this->check_authentication();
        if (!$id) {
            redirect("users/0", true);
        }
        $this->User_model->remove_row($id);
        redirect('users/0');
        //print_r($this->input->post());exit;
    }

    function check_email_exist($requested_email) {// this is the custom call back function in user_creation
        $this->load->modle('user_modle');
        $emial_not_used = $this->User_modle->check_email_exist($requested_email);
        $response = 0;
        if ($response != 0) {
            $response = 1;
        } else {
            $response;
        }
    }
    //   paging Exapmle by sir
    


    
    public function users_data() {
        $data = NULL;
        $this->check_authentication();
        $datetime = $this->get_last_login_detail();
        // Load pagination class library
        $this->load->library('pagination');
        // Configure the pagination variables
        $config['base_url'] = $this->config->base_url() . 'users/';
        $config['total_rows'] = $this->User_model->record_count();
        $config['per_page'] = 10;
        $config['first_url'] = $this->config->base_url() . 'users/0';
        $this->pagination->initialize($config);
        //$config[‘uri_segment’] = 2;
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data['datetime']=$datetime; 
        $data['links'] = $this->pagination->create_links();
        $data['rec'] = $this->User_model->get_all_users_data($config['per_page'], $page);
        //echo "<pre>";print_r($data);exit;
        $this->load->view("information_1", $data);
    }

    //=======================date picker
    function datepicker() {
        $this->load->view("datepicker");
    }

    //=======storing date in format january 1 2016
    function createdate() {
        if ($this->input->server("REQUEST_METHOD") == "POST") {
            //  echo "<pre>";print_r($this->input->post());exit;
            $db_response = $this->User_model->create_date($this->input->post());
            //  echo "Db response=".$db_response;exit;
            if ($db_response) {
                $response = 1;
            } else {
                $response = 0;
            }
            echo $response;
        }
    }

    //echo "Db response=".$response;exit;
    //=======storing date in format january 1,2016
    function datepickertwo() {
        $this->load->view("datepickertwo");
    }

    function createdateformat() {
        if ($this->input->server("REQUEST_METHOD") == "POST") {
            $db_response = $this->User_model->create_date_format($this->input->post());
            if ($db_response) {
                $response = 1;
            } else {

                function viewdate() {
                    $query = $this->User_model->getdate();
                    $data['date'] = null;
                    if ($query) {
                        $data['date'] = $query;
                    }
                    // echo "<pre>";print_r($data['date']);exit;
                    $this->load->view('viewdate', $data);
                }

                $response = 0;
            }
            $this->load->view('viewdate', $data);
        }
    }

    /*     * *******
      View date from database Demo Exaple
     * ******* */

    function user_selected_date_format() {
        $this->check_authentication();
        $data = $this->session->all_userdata();
        // echo"<pre>";print_r ($data);
        $format = $data['date_format'];
        echo $format;
        if ($format == 1) {
            $this->load->view('calendar');
        } else {
            $this->load->view("datepicker");
        }
    }

    function lesson() {
        $this->load->view("lesson-2");
    }

    /*
     * add date to data base by User selected format
     */
//    function user_selected_date_format(){
//        
//        $this->load->view ('createdate');
//       
//    }
}
