<!DOCTYPE html>
<html>
    <head>
        <link href="<?php echo $this->config->base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" media="screen">
        <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/bootstrap.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>STUDENT</title>
        <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/validation.js"></script>
        <script>
            $('document').ready(function ()
            {
                /* validation */
                $("#register-form").validate({
                    rules:
                            {
                                user_password: {
                                    required: true,
                                    maxlength: 15
                                },
                                user_email: {
                                    required: true,
                                    email: true
                                },
                            },
                    messages:
                            {
                                user_password: {
                                    required: "Provide a Password",
                                },
                                user_email: "Enter a Valid Email",
                            },
                    submitHandler: submitForm
                });
                /* validation */

                /* form submit */
                function submitForm()
                {
                    var data = $("#register-form").serialize();
                    $.ajax({
                        type: 'POST',
                        url: 'login-submit',
                        data: data,
                        beforeSend: function ()
                        {
                            $("#error").fadeOut();
                            $("#btn-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
                        },
                        success: function (data)
                        {
                            //alert(data);
                            if (data == true)
                            {
                                // use window.redirect method , return to memeber page
                                //setTimeout('$(".form-signin").fadeOut(500, function(){ $(".signin-form").load("successreg.php"); }); ',1000);
                                window.setTimeout(function () {
                                    window.location.href = '<?php echo $this->config->base_url(); ?>users/0';
                                }, 2000);
                            } else {
                                // stay here and show error message to user
                                //alert("Incorrect username/password. Please try again.");
                            }
                        }
                    });
                    return false;
                }
                /* form submit */
            });
        </script>
    </head>
    <body>
        <div class="container">
            <div class="signin-form">
                <h3>Login Form</h3> 
                <?php
                echo form_open('class ="form-horizontal" class="form-signin" id ="register-form"');
                ?>
                <div class="form-group">
                    <?php
                    $data = array(
                        'type' => 'email',
                        'name' => 'user_email',
                        'class' => 'form-control',
                        'id' => 'user_email',
                        'placeholder' => 'Email address'
                    );
                    echo form_input($data)
                    ?> 
                    <span id="check-e"></span>
                </div>
                <div class="form-group">
                    <?php
                    $data = array(
                        'type' => 'password',
                        'name' => 'user_password',
                        'class' => 'form-control',
                        'id' => 'password',
                        'placeholder' => 'password'
                    );
                    echo form_input($data)
                    ?> 
                </div>
                <?php
                $data = array(
                    'type' => 'submit',
                    'class' => 'btn btn-default',
                    'content' => '<span class="glyphicon glyphicon-log-in"></span> &nbsp;Login',
                    'name' => 'Login',
                    'id' => 'button',
                    'value' => 'submit'
                );
                echo form_button($data);
                ?>
<?php echo form_close(); ?>
                <br />       
<?php echo anchor('/signup', 'Create New Account', 'title="create new account for user"'); ?>
            </div>

        </div>
        <div class ="centre">         
            <div class="container">
<?php include_once('includes/footer.php') ?>