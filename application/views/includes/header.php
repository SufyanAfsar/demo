<!DOCTYPE html>
<html>
    <head>
        <link href="<?php echo $this->config->base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" media="screen">
        <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/bootstrap.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>STUDENT</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="well">
                        <ul class= "nav nav-tabs"> 
                            <li >  
                                <?php echo anchor('users/0', 'Home', 'title="index/0"'); ?></span></a>
                            </li>

                            <li >
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Teacher<span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><?php echo anchor('teacher', 'Add Teacher', 'title="Teacher"'); ?></li>
                                    <li><?php echo anchor('teacher-info/0', 'View Teacher', 'title="Teacher"'); ?></li>

                                </ul>
                            </li>
                            <li >
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Student<span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><?php echo anchor('student', 'Add Students', 'title="Student"'); ?></li>
                                    <li><?php echo anchor('#', 'View Students', 'title="Student"'); ?></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Class<span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><?php echo anchor('class', 'Add Class', 'title="Class"'); ?></li>
                                    <li><?php echo anchor('class-info/0', 'View Class', 'title="Class"'); ?></li>
                                </ul>
                            </li>
                            <li >
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Subject <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><?php echo anchor('subject', 'Add Subjects', 'title="Teacher"'); ?> </li>
                                    <li><?php echo anchor('#', 'View Subjects', 'title="Teacher"'); ?></li>
                                </ul>
                            </li>
                            <li >
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Date <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><?php echo anchor('calendar', 'Add Date ', 'title="Date"'); ?> </li>
                                    <li><?php echo anchor('viewdate', 'View Date', 'title="Teacher"'); ?></li>
                                </ul>
                            </li>
                            <li >  
                                <?php echo anchor('history', 'history', 'title="history"'); ?></span></a>
                            </li>
                            <li><?php echo anchor('logout', 'Logout', 'title="Logout"'); ?> </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class ="centre">
            <div class="container">