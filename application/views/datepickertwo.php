<!DOCTYPE html>
<html>
<head>
    <title>datepickertwo</title>
    <link href="<?php echo $this->config->base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="<?php echo $this->config->base_url();?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body>
<div class="container">
<?php echo form_open('createdatetwo','class ="from" class="form-signin" id ="register-form"');       
     ?>     
            <legend>Jan 21, 2017</legend>
                    <div class="control-group">
                    <div class="controls input-append date form_date" data-date="" data-date-format=" M d, yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                    <input size="16" name="date" type="text" value="" readonly>
                    <span class="add-on"><i class="icon-remove"></i></span>
                    <span class="add-on"><i class="icon-th"></i></span>
                 </div>
                 </div>
                 <?php $data = array (
                                'type'=>'submit',
                                'class'=>'btn btn-default',
                                'content'=> '<span class="glyphicon glyphicon-log-in"></span> &nbsp; Create Account',
                                'name'=>'btn-save',
                                'id'=>'button',
                                'value'=>'submit'
                                );
                                echo form_button($data);
                                ?>
                <?php echo form_close(); ?>   
    </form>
</div>
<script charset="UTF-8" type="text/javascript" src="<?php echo $this->config->base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->base_url();?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" charset="UTF-8" src="<?php echo $this->config->base_url();?>assets/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript">
   
	$('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });

</script>
</body>
</html>
