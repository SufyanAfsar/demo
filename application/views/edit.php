 <?php include_once('includes/header.php') ?>
     <?php echo form_open('index','class ="from" class="form-signin" id ="register-form"');       
     ?>
            <h2 class="form-signin-heading">Please Provide Inforamtion </h2><hr />
            <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=>'fname',
				'class'=>'form-control',
				'id'=>'fname',
                                'placeholder'=>'First name',
                                'value'=>   $user_fname          
				 );
				 echo form_input($data)
				 ?> 
            </div>
             <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=>'lname',
				'class'=>'form-control',
				'id'=>'lname',
				'placeholder'=>'Last name',
                                'value'=>   $user_lname			
				 );
				 echo form_input($data)
				 ?> 
            </div>
            <div class="form-group">
                <?php
                $explode_date = explode("-", $user_dob);
               // echo "<pre>"; print_r($explode_date); exit;
                $create_date = $explode_date[0].'/'.$explode_date[1].'/'.$explode_date[2];
                //echo $create_date;exit;
                ?>
                 <?php $data = array ( 
				'type'=>'user_dob',
				'name'=>'dob',
				'class'=>'form-control',
				'id'=>'dob',
                                'value' => $create_date
				 );
				 echo form_input($data)
				 ?> 
            </div>
            <div class="form-group">
             <?php $data = array ( 
				'type'       =>'email',
				'name'       =>'user_email',
				'class'      =>'form-control',
				'id'         =>'user_email',
				'placeholder'=>'Email address',
                                'value'      =>$user_email
				 );
				 echo form_input($data)
				 ?> 
            <span id="check-e"></span>
            </div>
            <div class="form-group">
             	<?php $data = array ( 
				'type'       =>'password',
				'name'       =>'password',
				'class'      =>'form-control',
				'id'         =>'password',
				'placeholder'=>'Please Chose new password',
				 );
				 echo form_input($data)
				 ?> 
            </div>
            <hr />  
                <?php $data = array (
                                'type'=>'submit',
                                'class'=>'btn btn-default',
                                'content'=> '<span class="glyphicon glyphicon-log-in"></span> &nbsp; Update Account',
                                'name'=>'btn-save',
                                'id'=>'button',
                                'value'=>'submit'
                                );
                                echo form_button($data);
                                ?>
            <input type="hidden" name="user_id" value="<?php echo $user_id?>">
                <?php echo form_close(); ?>
            
            </div>
        </form>
    </div>
     <?php include_once('includes/footer.php')?>