<?php 
include_once(APP_VIEW_PATH.'includes/header.php') 
?>
<script type="text/javascript" src="<?php echo $this->config->base_url();?>assets/js/validation.js">
</script>
<script>
$('document').ready(function()
{
    /* validation */
    $("#register-form").validate({
        rules:
        {
            sub_name: {
                required: true,
                minlength: 3
            },
	    sub_editor: {
                required: true,
                minlength: 1
            },
        },
        messages:
        {
            tech_name: "Please Enter Class Name",
            tech_designation: "Please Enter Section Name ",
        },
        submitHandler: submitForm
    });
    /* validation */

    /* form submit */
    function submitForm()
    {
        var data = $("#register-form").serialize();
        $.ajax({
            type : 'POST',
            url  : '<?php echo $this->config->base_url();?>class-submit',
            data : data,
            beforeSend: function()
            {
                $("#error").fadeOut();
                $("#btn-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
            },
            success :  function(data)
            {
                //alert(data);
                if(data == true){
                    // use window.redirect method , return to memeber page
                    //setTimeout('$(".form-signin").fadeOut(500, function(){ $(".signin-form").load("successreg.php"); }); ',1000);
                    window.setTimeout(function(){ window.location.href = '<?php echo $this->config->base_url();?>class-info/0'; }, 500);
                    
                } else {
                    // stay here and show error message to user'
                    //alert("data passed but returning zero");
                }
            }
        });
        return false;
    }
    /* form submit */
});
</script>
<div class="signin-form">
    <div class="container">
     <?php echo form_open('#','class ="from" class="form-signin" id ="register-form"');       
     ?>
            <h2 class="form-signin-heading">Provide Student  Data </h2><hr />
            <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=> 'sub_name',
				'class'=>'form-control',
				'id'=>'sub_name',
				'placeholder'=>'Subject Name'			
				 );
				 echo form_input($data)
				 ?> 
            </div>
             <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=> 'sub_editor' ,
				'class'=>'form-control',
				'id'=>'sub_editor',
				'placeholder'=>'Publisher Name'			
				 );
				 echo form_input($data)
				 ?> 
            </div>
              
            <hr />  
                <?php $data = array (
                                'type'=>'submit',
                                'class'=>'btn btn-default',
                                'content'=> '<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add Subject',
                                'name'=>'btn-save',
                                'id'=>'button',
                                'value'=>'submit'
                                );
                                echo form_button($data);
                                ?>
                <?php echo form_close(); ?>
            </div>
        </form>
    </div>
 <?php 
include_once(APP_VIEW_PATH.'includes/footer.php') 
?>

