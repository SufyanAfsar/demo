
<?php 
include_once(APP_VIEW_PATH.'includes/header.php') 
?>
  <script type="text/javascript" src="<?php echo $this->config->base_url();?>assets/js/validation.js"></script>
<script>
$('document').ready(function()
{
    /* validation */
    $("#register-form").validate({
        rules:
        {
            tech_name: {
                required: true,
                minlength: 3
            },
	    tech_designation: {
                required: true,
                minlength: 3
            },
        },
        messages:
        {
            tech_name: "Please Enter teacher Name",
            tech_designation: "Please Enter teacher Designation ",
              
            
        },
        submitHandler: submitForm
    });
    /* validation */

    /* form submit */
    function submitForm()
    {
        var data = $("#register-form").serialize();
        $.ajax({
            type : 'POST',
            url  : '<?php echo $this->config->base_url();?>teacher-submit',
            data : data,
            beforeSend: function()
            {
                $("#error").fadeOut();
                $("#btn-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
            },
            success :  function(data)
            {
                //alert(data);
                if(data == true){
                    // use window.redirect method , return to memeber page
                    //setTimeout('$(".form-signin").fadeOut(500, function(){ $(".signin-form").load("successreg.php"); }); ',1000);
                    window.setTimeout(function(){ window.location.href = '<?php echo $this->config->base_url();?>teacher-info/0'; }, 500);
                    
                } else {
                    // stay here and show error message to user'
                    //alert("data passed but returning zero");
                }
            }
        });
        return false;
    }
    /* form submit */
});
</script>

<div class="signin-form">
    <div class="container">
     <?php echo form_open('#','class ="from" class="form-signin" id ="register-form"');       
     ?>
            <h2 class="form-signin-heading">Provide Student Data </h2><hr />
            <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=> 'std_fname',
				'class'=>'form-control',
				'id'=>'std_fname',
				'placeholder'=>'First Name'			
				 );
				 echo form_input($data)
				 ?> 
            </div>
             <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=> 'std_lname' ,
				'class'=>'form-control',
				'id'=>'std_lname',
				'placeholder'=>'Last Name'			
				 );
				 echo form_input($data)
				 ?> 
            </div>
             <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=> 'std_parent' ,
				'class'=>'form-control',
				'id'=>'Parent Name',
				'placeholder'=>'Parent'			
				 );
				 echo form_input($data)
				 ?> 
            </div>
             <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=> 'class_id' ,
				'class'=>'form-control',
				'id'=>'class_id',
				'placeholder'=>'Select Class'			
				 );
				 echo form_dropdown('class_level',$class,set_value('class_level'),$data);
				 ?> 
            </div>
            <hr />  
                <?php $data = array (
                                'type'=>'submit',
                                'class'=>'btn btn-default',
                                'content'=> '<span class="glyphicon glyphicon-log-in"></span> &nbsp;Add Student',
                                'name'=>'btn-save',
                                'id'=>'button',
                                'value'=>'submit'
                                );
                                echo form_button($data);
                                ?>
                <?php echo form_close(); ?>
            </div>
        </form>
    </div>

 <?php 
include_once(APP_VIEW_PATH.'includes/footer.php') 
?>
