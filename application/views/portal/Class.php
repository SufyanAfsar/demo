<?php 
include_once(APP_VIEW_PATH.'includes/header.php') 
?>
<script type="text/javascript" src="<?php echo $this->config->base_url();?>assets/js/validation.js">
</script>
<script>
$('document').ready(function()
{
    /* validation */
    $("#register-form").validate({
        rules:
        {
            class_name: {
                required: true,
                minlength: 3
            },
	    class_level: {
                required: true,
                minlength: 1
            },
             teacher: {
                required: true,
                minlength: 1
            },
        },
        messages:
        {
            class_name: "Please Enter Class Name",
            class_level: "Please Enter Level ",
            teacher: "Please select teacher name ",       
        },
        submitHandler: submitForm
    });
    /* validation */

    /* form submit */
    function submitForm()
    {
        var data = $("#register-form").serialize();
        $.ajax({
            type : 'POST',
            url  : '<?php echo $this->config->base_url();?>class-submit',
            data : data,
            beforeSend: function()
            {
                $("#error").fadeOut();
                $("#btn-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
            },
            success :  function(data)
            {
                //alert(data);
                if(data == true){
                    // use window.redirect method , return to memeber page
                    //setTimeout('$(".form-signin").fadeOut(500, function(){ $(".signin-form").load("successreg.php"); }); ',1000);
                    window.setTimeout(function(){ window.location.href = '<?php echo $this->config->base_url();?>class-info/0'; }, 500);
                    
                } else {
                    // stay here and show error message to user'
                  //  alert("data passed but returning zero");
                }
            }
        });
        return false;
    }
    /* form submit */
});
</script>
<div class="signin-form">
    <div class="container">
     <?php echo form_open('#','class ="from" class="form-signin" id ="register-form"');       
     ?>
            <h2 class="form-signin-heading">Provide Class Data </h2><hr />
            <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=> 'class_name',
				'class'=>'form-control',
				'id'=>'fname',
				'placeholder'=>'Class Name'			
				 );
				 echo form_input($data)
				 ?> 
            </div>
             <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=> 'class_level' ,
				'class'=>'form-control',
				'id'=>'lname',
				'placeholder'=>'Level'			
				 );
				 echo form_input($data)
				 ?> 
            </div>
               
         <div class="form-group">
             <div class="form-group">
                 <?php $attributes  = array ( 
                            'type'=>'text',
                            'name'=> 'sub_name' ,
                            'class'=>'form-control',
                            'id'=>'sub_name',
                            'placeholder'=>'Subject'			
                             );
                              echo form_dropdown('subject', $subject, set_value('subject'), $attributes); ?>	
            </div>
                 <?php $attributes  = array ( 
				'type'=>'text',
				'name'=> 'tech_name' ,
				'class'=>'form-control',
				'id'=>'teacher',
				'placeholder'=>'teacher'			
				 );
				  echo form_dropdown('teacher', $teacher, set_value('teacher'), $attributes); ?>	
                </div>
            <hr />  
                <?php $data = array (
                                'type'=>'submit',
                                'class'=>'btn btn-default',
                                'content'=> '<span class="glyphicon glyphicon-log-in"></span> &nbsp; Add Class',
                                'name'=>'btn-save',
                                'id'=>'button',
                                'value'=>'submit'
                                );
                                echo form_button($data);
                                ?>
                <?php echo form_close(); ?>
            </div>
        </form>
    </div>
<?php
$source = '2012-07-6';
$date = new DateTime($source);
echo $date->format('F.d.Y'); // 31.07.2012?>
<br />


 <?php 
include_once(APP_VIEW_PATH.'includes/footer.php') 
?>

