<?php 
include_once(APP_VIEW_PATH.'includes/header.php') 
?>

<h2>Teacher information</h2>
<div class ="container">
    <table class ="table">
        <tr>
            <th>Name </th>
            <th>Designation</th>
        </tr>
        <?php foreach ($results as $row ): ?>
        <tr>
            <td> <?= $row-> tech_name ?> </td>
            <td> <?= $row-> tech_designation ?> </td>
        </tr>
        <?php endforeach; ?>
    </table>
    <h2><p><?php echo $links; ?></p></h2>
</div>

<?php 
include_once(APP_VIEW_PATH.'includes/footer.php') 
?>
