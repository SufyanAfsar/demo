 

<?php include_once('includes/header.php') ?>
 <script type="text/javascript" src="<?php echo $this->config->base_url();?>assets/js/validation.js"></script>

<script>
$('document').ready(function()
{
    /* validation */
    $("#register-form").validate({
        rules:
        {
            fname: {
                required: true,
                minlength: 3
            },
	    lname: {
                required: true,
                minlength: 3
            },
            password: {
                required: true,
             
                maxlength: 15
            },
            cpassword: {
                required: true,
                equalTo: '#password'
            },
            user_email: {
                required: true,
                email: true
            },
        },
        messages:
        {
            fname: "please provide your First Name",
            lname: "please provide your Last Name ",
              dob:    "please enter the DOB",
            password:{
            required: "Provide a Password",
            minlength: "Password Needs To Be Minimum of 8 Characters"
            },
            user_email: "Enter a Valid Email",
            cpassword:{
            required: "Retype Your Password",
            equalTo: "Password Mismatch! Retype"
            }
        },
        submitHandler: submitForm
    });
    /* validation */

    /* form submit */
    function submitForm()
    {
        var data = $("#register-form").serialize();
        $.ajax({
            type : 'POST',
            url  : '<?php echo $this->config->base_url();?>update-submit',
                
            data : data,
            beforeSend: function()
            {
                $("#error").fadeOut();
                $("#btn-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
            },
            success :  function(data)
            {
                 
                if(data == true){
             //       alert(data);
                    // use window.redirect method , return to memeber page
                    //setTimeout('$(".form-signin").fadeOut(500, function(){ $(".signin-form").load("successreg.php"); }); ',1000);
                    window.setTimeout(function(){ window.location.href = '<?php echo $this->config->base_url();?>index/0'; }, 500);
                    //alert("Now please login again to view the updated acount! ")
                } else {
                    // stay here and show error message to user'
                    //alert("data passed but returning zero");
                }
            }
        });
        return false;
    }
    /* form submit */
});
</script>

<div class="signin-form">
    <div class="container">
     <?php echo form_open('#','class ="from" class="form-signin" id ="register-form"');       
     ?>
            <h2 class="form-signin-heading">Please Provide Inforamtion </h2><hr />
            <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=>'fname',
				'class'=>'form-control',
				'id'=>'fname',
                                'placeholder'=>'First name',
                                'value'=>   $user_fname          
				 );
				 echo form_input($data)
				 ?> 
            </div>
             <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=>'lname',
				'class'=>'form-control',
				'id'=>'lname',
				'placeholder'=>'Last name',
                                'value'=>   $user_lname			
				 );
				 echo form_input($data)
				 ?> 
            </div>
            <div class="form-group">
                <?php
                $explode_date = explode("-", $user_dob);
            //    echo "<pre>"; print_r($explode_date); exit;
                $create_date = $explode_date[0].'/'.$explode_date[1].'/'.$explode_date[2];
                //echo $create_date;exit;
                ?>
                 <?php $data = array ( 
				'type'=>'user_dob',
				'name'=>'dob',
				'class'=>'form-control',
				'id'=>'dob',
                                'value' => $create_date
				 );
				 echo form_input($data)
				 ?> 
            </div>
            <div class="form-group">
             <?php $data = array ( 
				'type'       =>'email',
				'name'       =>'user_email',
				'class'      =>'form-control',
				'id'         =>'user_email',
				'placeholder'=>'Email address',
                                'value'      =>$user_email
				 );
				 echo form_input($data)
				 ?> 
            <span id="check-e"></span>
            </div>
            <div class="form-group">
             	<?php $data = array ( 
				'type'       =>'password',
				'name'       =>'password',
				'class'      =>'form-control',
				'id'         =>'password',
				'placeholder'=>'Please Chose new password',
				 );
				 echo form_input($data)
				 ?> 
            </div>
            <div class="form-group">
             	<?php $data = array ( 
				'type'=>'password',
				'name'=>'cpassword',
				'class'=>'form-control',
				'id'=>'cpassword',
				'placeholder'=>'Confirm password'			
				 );
				 echo form_input($data)
				 ?>             
            </div>
            <hr />  
                <?php $data = array (
                                'type'=>'submit',
                                'class'=>'btn btn-default',
                                'content'=> '<span class="glyphicon glyphicon-log-in"></span> &nbsp; Update Account',
                                'name'=>'btn-save',
                                'id'=>'button',
                                'value'=>'submit'
                                );
                                echo form_button($data);
                                ?>
            <input type="hidden" name="user_id" value="<?php echo $user_id?>">
                <?php echo form_close(); ?>
            
            </div>
        </form>
    </div>

 <?php include_once('includes/footer.php')?>
