
 <!DOCTYPE html>
<html>
    <head>
        <link href="<?php echo $this->config->base_url();?>assets/css/bootstrap.css" rel="stylesheet" media="screen">
       <script charset="UTF-8" type="text/javascript" src="<?php echo $this->config->base_url();?>assets/js/jquery-3.1.1.min.js"></script>
          <script type="text/javascript" src="<?php echo $this->config->base_url();?>assets/js/bootstrap.js"></script>
          <script type="text/javascript" src="<?php echo $this->config->base_url();?>assets/js/validation.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>STUDENT</title>
      
    
<script>
$('document').ready(function()
{
    /* validation */
    $("#register-form").validate({
        rules:
        {
            fname: {
                required: true,
                minlength: 3
            },
	    lname: {
                required: true,
                minlength: 3
            },
            password: {
                required: true,
             
                maxlength: 15
            },
            cpassword: {
                required: true,
                equalTo: '#password'
            },
            user_email: {
                required: true,
                email: true
            },
        },
        messages:
        {
            fname: "please provide your First Name",
            lname: "please provide your Last Name ",
              dob:    "please enter the DOB",
            password:{
            required: "Provide a Password",
            minlength: "Password Needs To Be Minimum of 8 Characters"
            },
            user_email: "Enter a Valid Email",
            cpassword:{
            required: "Retype Your Password",
            equalTo: "Password Mismatch! Retype"
            }
        },
        submitHandler: submitForm
    });
    /* validation */

    /* form submit */
    function submitForm()
    {
        var data = $("#register-form").serialize();
        $.ajax({
            type : 'POST',
            url  : '<?php echo $this->config->base_url();?>signup-submit',
            data : data,
            beforeSend: function()
            {
                $("#error").fadeOut();
                $("#btn-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
            },
            success :  function(data)
            {
                //alert(data);
                if(data == true){
                    // use window.redirect method , return to memeber page
                    //setTimeout('$(".form-signin").fadeOut(500, function(){ $(".signin-form").load("successreg.php"); }); ',1000);
                    window.setTimeout(function(){ window.location.href = '<?php echo $this->config->base_url();?>'; }, 500);
                    alert("Now please login ")
                } else {
                    // stay here and show error message to user'
                    //alert("data passed but returning zero");
                }
            }
        });
        return false;
    }
    /* form submit */
});
</script>

</head>
<body>

<div class="signin-form">
    <div class="container">
     <?php echo form_open('#','class ="from" class="form-signin" id ="register-form"');       
     ?>
            <h2 class="form-signin-heading">Provide Inforamtion </h2><hr />
            <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=> 'lname',
				'class'=>'form-control',
				'id'=>'fname',
				'placeholder'=>'First name'			
				 );
				 echo form_input($data)
				 ?> 
            </div>
             <div class="form-group">
                 <?php $data = array ( 
				'type'=>'text',
				'name'=> 'fname' ,
				'class'=>'form-control',
				'id'=>'lname',
				'placeholder'=>'Last name'			
				 );
				 echo form_input($data)
				 ?> 
            </div>
              <div class="form-group">
             <?php 
                    $options = array(
                     '0'        => 'select format',
                    '1'        => 'January 21, 2017',
                    '2'        => 'Jan 21, 2017',
                    '3'        => '21-jan-2017',
                    '4'        => '21/01/2017',
                           );
                    echo form_dropdown('value', $options);
				 ?> 
            <span id="check-e"></span>
            </div>
            <div class="form-group">
                 <?php $data = array ( 
				'type'=>'date',
				'name'=>'date',
				'class'=>'form-control',
				'id'=>'dob'			
				 );
				 echo form_input($data)
				 ?> 
            </div>
            <div class="form-group">
             <?php $data = array ( 
				'type'=>'email',
				'name'=>'user_email',
				'class'=>'form-control',
				'id'=>'user_email',
				'placeholder'=>'Email address'			
				 );
				 echo form_input($data)
				 ?> 
            <span id="check-e"></span>
            </div>
           
             
             
            <div class="form-group">
             	<?php $data = array ( 
				'type'=>'password',
				'name'=>'password',
				'class'=>'form-control',
				'id'=>'password',
				'placeholder'=>'password'			
				 );
				 echo form_input($data)
				 ?> 
            </div>
            <div class="form-group">
             	<?php $data = array ( 
				'type'=>'password',
				'name'=>'cpassword',
				'class'=>'form-control',
				'id'=>'cpassword',
				'placeholder'=>'Retype password'			
				 );
				 echo form_input($data)
				 ?>             
            </div>
            <hr />  
                <?php $data = array (
                                'type'=>'submit',
                                'class'=>'btn btn-default',
                                'content'=> '<span class="glyphicon glyphicon-log-in"></span> &nbsp; Create Account',
                                'name'=>'btn-save',
                                'id'=>'button',
                                'value'=>'submit'
                                );
                                echo form_button($data);
                                ?>
                <?php echo form_close(); ?>
            </div>
        </form>
    </div>
</div>
<div class ="centre">
            
     <div class="container">
 <?php include_once('includes/footer.php')?>