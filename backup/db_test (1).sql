-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2016 at 10:12 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_class`
--

CREATE TABLE `tbl_class` (
  `class_id` int(11) NOT NULL,
  `class_section` varchar(10) NOT NULL,
  `cls_level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_class`
--

INSERT INTO `tbl_class` (`class_id`, `class_section`, `cls_level`) VALUES
(2, 'B', 'X'),
(3, 'A', 'IX'),
(4, 'A', 'XI'),
(5, 'B', 'XII'),
(7, 'A', 'VIII'),
(8, 'B', '10 th');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_class_subject`
--

CREATE TABLE `tbl_class_subject` (
  `id` int(11) NOT NULL,
  `sub_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_class_subject`
--

INSERT INTO `tbl_class_subject` (`id`, `sub_id`, `class_id`) VALUES
(1, 1, 2),
(2, 2, 2),
(3, 3, 2),
(4, 3, 5),
(5, 3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `std_id` int(11) NOT NULL,
  `std_fname` varchar(20) NOT NULL,
  `std_lname` varchar(20) NOT NULL,
  `std_parent` varchar(30) NOT NULL,
  `class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`std_id`, `std_fname`, `std_lname`, `std_parent`, `class_id`) VALUES
(1, 'ali', 'ayaz', 'nawaz', 2),
(2, 'fayz', 'khan', 'khan', 2),
(3, 'hussain', 'hussain', 'khan', 3),
(4, 'khan', 'hussain', 'neemat', 4),
(5, 'neemat', 'khan', 'hussain', 5),
(6, 'fayz', 'khan', 'khan', 3),
(7, 'hussain', 'hussain', 'khan', 3),
(8, 'khan', 'hussain', 'neemat', 4),
(9, 'neemat', 'khan', 'hussain', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_subject`
--

CREATE TABLE `tbl_student_subject` (
  `std_id` int(11) NOT NULL,
  `sub_id` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_subject`
--

INSERT INTO `tbl_student_subject` (`std_id`, `sub_id`, `id`) VALUES
(1, 1, 1),
(1, 2, 2),
(1, 1, 3),
(1, 2, 4),
(4, 4, 5),
(4, 3, 6),
(4, 2, 7),
(2, 2, 8),
(2, 1, 9),
(4, 4, 10),
(4, 3, 11),
(4, 2, 12),
(2, 2, 13),
(2, 1, 14);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_teacher`
--

CREATE TABLE `tbl_student_teacher` (
  `id` int(11) NOT NULL,
  `std_id` int(11) NOT NULL,
  `tech_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_teacher`
--

INSERT INTO `tbl_student_teacher` (`id`, `std_id`, `tech_id`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subject`
--

CREATE TABLE `tbl_subject` (
  `sub_id` int(11) NOT NULL,
  `sub_name` varchar(20) NOT NULL,
  `sub_editor` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subject`
--

INSERT INTO `tbl_subject` (`sub_id`, `sub_name`, `sub_editor`) VALUES
(1, 'Physics', 'Pnunjab book board'),
(2, 'Bio', 'Pnunjab book board'),
(3, 'Chemistry', 'Pnunjab book board'),
(4, 'Maths', 'Pnunjab book board');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teacher`
--

CREATE TABLE `tbl_teacher` (
  `tech_id` int(11) NOT NULL,
  `tech_name` varchar(20) NOT NULL,
  `tech_designation` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_teacher`
--

INSERT INTO `tbl_teacher` (`tech_id`, `tech_name`, `tech_designation`) VALUES
(2, 'Asif', 'teacher'),
(12, 'jillani', 'Teacher'),
(13, 'Sajid', 'Teacher'),
(14, 'sufyan', 'Teacher'),
(15, 'Farhan', 'Teacher'),
(16, 'Ali', 'teacher visitor');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tech_class`
--

CREATE TABLE `tbl_tech_class` (
  `id` int(11) NOT NULL,
  `tech_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_tech_class`
--

INSERT INTO `tbl_tech_class` (`id`, `tech_id`, `class_id`) VALUES
(1, 2, 2),
(2, 2, 3),
(3, 12, 5),
(4, 12, 5),
(5, 13, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tech_subject`
--

CREATE TABLE `tbl_tech_subject` (
  `id` int(11) NOT NULL,
  `tech_id` int(11) NOT NULL,
  `sub_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_tech_subject`
--

INSERT INTO `tbl_tech_subject` (`id`, `tech_id`, `sub_id`) VALUES
(1, 2, 2),
(2, 13, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_lname` varchar(20) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_dob` date NOT NULL,
  `user_fname` varchar(20) NOT NULL,
  `user_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_lname`, `user_password`, `user_dob`, `user_fname`, `user_email`) VALUES
(130, 'ssss', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(131, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(132, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(133, 'sdfasdfsaf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(134, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(135, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(136, 'eeee', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'SDSDSDS', 'sajid@gmail.com'),
(137, 'ddddd', '3b712de48137572f3849aabd5666a4e3', '2016-12-23', 'dddd', 'sajid@gmail.com'),
(138, 'ssss', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(139, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(140, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(141, 'sdfasdfsaf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(142, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(143, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(144, 'eeee', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'SDSDSDS', 'sajid@gmail.com'),
(145, 'ddddd', '3b712de48137572f3849aabd5666a4e3', '2016-12-23', 'dddd', 'sajid@gmail.com'),
(146, 'ssss', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(147, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(148, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(149, 'sdfasdfsaf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(150, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(151, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(152, 'eeee', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'SDSDSDS', 'sajid@gmail.com'),
(153, 'ddddd', '3b712de48137572f3849aabd5666a4e3', '2016-12-23', 'dddd', 'sajid@gmail.com'),
(154, 'ssss', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(155, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(156, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(157, 'sdfasdfsaf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sss', 'maziz@gmail.com'),
(158, 'SHOAIB', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'ali', 'sajid@gmail.com'),
(159, 'sdfasdf', '698d51a19d8a121ce581499d7b701668', '2016-12-23', 'sdfasdf', 'sajid@gmail.com'),
(160, 'sufyan', '3b712de48137572f3849aabd5666a4e3', '2016-12-26', 'afsar', 'sufyan@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `tbl_class`
--
ALTER TABLE `tbl_class`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `tbl_class_subject`
--
ALTER TABLE `tbl_class_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`std_id`);

--
-- Indexes for table `tbl_student_subject`
--
ALTER TABLE `tbl_student_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student_teacher`
--
ALTER TABLE `tbl_student_teacher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_subject`
--
ALTER TABLE `tbl_subject`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `tbl_teacher`
--
ALTER TABLE `tbl_teacher`
  ADD PRIMARY KEY (`tech_id`);

--
-- Indexes for table `tbl_tech_class`
--
ALTER TABLE `tbl_tech_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_tech_subject`
--
ALTER TABLE `tbl_tech_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_class`
--
ALTER TABLE `tbl_class`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_class_subject`
--
ALTER TABLE `tbl_class_subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
  MODIFY `std_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_student_subject`
--
ALTER TABLE `tbl_student_subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_student_teacher`
--
ALTER TABLE `tbl_student_teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_subject`
--
ALTER TABLE `tbl_subject`
  MODIFY `sub_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_teacher`
--
ALTER TABLE `tbl_teacher`
  MODIFY `tech_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_tech_class`
--
ALTER TABLE `tbl_tech_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_tech_subject`
--
ALTER TABLE `tbl_tech_subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
