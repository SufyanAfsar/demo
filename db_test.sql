-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 26, 2017 at 03:17 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_test`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetAllNames` ()  BEGIN
SELECT * from tbl_student;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetTotalNames` ()  BEGIN
    DECLARE total INT DEFAULT 0;
SELECT count(*) into total
from tbl_student;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `name-of-std` (IN `studentnamereading` INT UNSIGNED)  NO SQL
BEGIN
 SELECT std_id,std_parent
 FROM tbl_student 
 WHERE std_id = studentnamereading;
 
 END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `book_price` varchar(255) NOT NULL,
  `book_author` varchar(255) NOT NULL,
  `book_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `created_at`, `updated_at`, `book_price`, `book_author`, `book_name`) VALUES
(6, 'pakistan', 'pakistan', '2017-01-27 15:22:03', '2017-01-30 16:18:31', '', '', ''),
(7, 'google', 'pakistan', '2017-01-30 14:17:49', '2017-01-30 14:17:49', '', '', ''),
(8, 'y', 'yy', '2017-01-31 11:48:01', '2017-01-31 11:48:01', '', '', ''),
(9, 'book', 'pakistani', '2017-01-31 12:24:20', '2017-01-31 14:25:31', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ip2location_db11`
--

CREATE TABLE `ip2location_db11` (
  `ip_from` int(10) UNSIGNED DEFAULT NULL,
  `ip_to` int(10) UNSIGNED DEFAULT NULL,
  `country_code` char(2) DEFAULT NULL,
  `country_name` varchar(64) DEFAULT NULL,
  `region_name` varchar(128) DEFAULT NULL,
  `city_name` varchar(128) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `zip_code` varchar(30) DEFAULT NULL,
  `time_zone` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ip2location_db11`
--

INSERT INTO `ip2location_db11` (`ip_from`, `ip_to`, `country_code`, `country_name`, `region_name`, `city_name`, `latitude`, `longitude`, `zip_code`, `time_zone`) VALUES
(0, 16777215, '-', '-', '-', '-', 0, 0, '-', '-'),
(16777216, 16777471, 'AU', 'Australia', 'Queensland', 'Brisbane', -27.46794, 153.02809, '4000', '+10:00'),
(16777472, 16778239, 'CN', 'China', 'Fujian', 'Fuzhou', 26.06139, 119.30611, '350004', '+08:00'),
(3758091264, 3758092287, 'CN', 'China', 'Shanghai', 'Shanghai', 31.22222, 121.45806, '200020', '+08:00'),
(3758092288, 3758093311, 'HK', 'Hong Kong', 'Hong Kong (SAR)', 'Hong Kong', 22.28552, 114.15769, '-', '+08:00'),
(3758093312, 3758094335, 'IN', 'India', 'Gujarat', 'Rajkot', 22.3, 70.78333, '383235', '+05:30'),
(3758094336, 3758095359, 'HK', 'Hong Kong', 'Hong Kong (SAR)', 'Hong Kong', 22.28552, 114.15769, '-', '+08:00'),
(3758095360, 3758095871, 'CN', 'China', 'Fujian', 'Fuzhou', 26.06139, 119.30611, '350004', '+08:00'),
(3758095872, 3758096127, 'SG', 'Singapore', 'Singapore', 'Singapore', 1.28967, 103.85007, '179431', '+08:00'),
(3758096128, 3758096383, 'AU', 'Australia', 'Queensland', 'Brisbane', -27.46794, 153.02809, '4000', '+10:00'),
(234973184, 234974207, 'PK', 'Pakistan', 'Sindh', 'Karachi', 24.9056, 67.0822, '12311', '+05:00'),
(288196608, 288197119, 'CA', 'Canada', 'Ontario', 'Toronto', 43.70011, -79.4163, 'M3B 0A3', '-04:00'),
(288197120, 288197631, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(288197632, 288198655, 'CL', 'Chile', 'Region Metropolitana de Santiago', 'Santiago', -33.42628, -70.56656, '7560742', '-04:00'),
(288198656, 288205311, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(288205312, 288205567, 'US', 'United States', 'Florida', 'Miami', 25.77427, -80.19366, '33010', '-04:00'),
(288205568, 288208895, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(288208896, 288210943, 'BR', 'Brazil', 'Sao Paulo', 'Sao Paulo', -23.5475, -46.63611, '01000-000', '-03:00'),
(288210944, 288212991, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(288212992, 288215039, 'CA', 'Canada', 'Quebec', 'Montreal', 45.50884, -73.58781, 'H1A 0A1', '-04:00'),
(288215040, 288219135, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(288219136, 288223231, 'US', 'United States', 'Florida', 'Orlando', 28.53834, -81.37924, '32817', '-04:00'),
(288223232, 288227327, 'CA', 'Canada', 'Ontario', 'Markham', 43.86682, -79.2663, 'L3P 0A1', '-04:00'),
(288227328, 289406975, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(289406976, 289603583, 'IE', 'Ireland', 'Dublin', 'Dublin', 53.34399, -6.26719, 'D8', '+01:00'),
(289603584, 289611775, 'GB', 'United Kingdom', 'England', 'Adlington', 53.32042, -2.13658, 'SK10 4NL', '+01:00'),
(289611776, 289613311, 'GB', 'United Kingdom', 'England', 'London', 51.50853, -0.12574, 'WC2N 5RJ', '+01:00'),
(289613312, 289615871, 'GB', 'United Kingdom', 'England', 'Adlington', 53.32042, -2.13658, 'SK10 4NL', '+01:00'),
(289615872, 289619967, 'FR', 'France', 'Auvergne-Rhone-Alpes', 'Grenoble', 45.16667, 5.71667, '38900', '+01:00'),
(289619968, 289624063, 'GB', 'United Kingdom', 'England', 'Adlington', 53.32042, -2.13658, 'SK10 4NL', '+01:00'),
(289624064, 289628159, 'CH', 'Switzerland', 'Zurich', 'Zurich', 47.36667, 8.55, '8099', '+02:00'),
(289628160, 289630207, 'GB', 'United Kingdom', 'England', 'Adlington', 53.32042, -2.13658, 'SK10 4NL', '+01:00'),
(289630208, 289632255, 'SE', 'Sweden', 'Skane lan', 'Malmoe', 55.60587, 13.00073, '21844', '+02:00'),
(289632256, 289652735, 'GB', 'United Kingdom', 'England', 'Adlington', 53.32042, -2.13658, 'SK10 4NL', '+01:00'),
(289652736, 289653759, 'NL', 'Netherlands', 'Noord-Holland', 'Amsterdam', 52.37403, 4.88969, '1089', '+02:00'),
(289653760, 289654783, 'GB', 'United Kingdom', 'England', 'Adlington', 53.32042, -2.13658, 'SK10 4NL', '+01:00'),
(289654784, 289655807, 'CZ', 'Czech Republic', 'Praha, Hlavni mesto', 'Prague', 50.08804, 14.42076, '150 00', '+02:00'),
(289655808, 289656831, 'DE', 'Germany', 'Bayern', 'Munich', 48.13743, 11.57549, '80331', '+02:00'),
(289656832, 289657855, 'BE', 'Belgium', 'Brussels Hoofdstedelijk Gewest', 'Brussels', 50.85045, 4.34878, '1210', '+02:00'),
(289657856, 289658879, 'AE', 'United Arab Emirates', 'Dubayy', 'Dubai', 25.25817, 55.30472, '-', '+04:00'),
(289658880, 289668095, 'GB', 'United Kingdom', 'England', 'Adlington', 53.32042, -2.13658, 'SK10 4NL', '+01:00'),
(289668096, 289669119, 'GB', 'United Kingdom', 'England', 'London', 51.50853, -0.12574, 'WC2N 5RJ', '+01:00'),
(289669120, 289734655, 'FR', 'France', 'Ile-de-France', 'Paris', 48.85341, 2.3488, '75000', '+01:00'),
(289734656, 289746943, 'GB', 'United Kingdom', 'England', 'Adlington', 53.32042, -2.13658, 'SK10 4NL', '+01:00'),
(289746944, 289757951, 'GB', 'United Kingdom', 'England', 'Bracknell', 51.41363, -0.75054, 'RG12 1HY', '+01:00'),
(289757952, 289758207, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(289758208, 289760255, 'GB', 'United Kingdom', 'England', 'Bracknell', 51.41363, -0.75054, 'RG12 1HY', '+01:00'),
(289760256, 289763327, 'DE', 'Germany', 'Mecklenburg-Vorpommern', 'Garden', 53.68888, 12.04874, '18276', '+02:00'),
(289763328, 289769727, 'GB', 'United Kingdom', 'England', 'Bracknell', 51.41363, -0.75054, 'RG12 1HY', '+01:00'),
(289769728, 289769983, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(289769984, 289773311, 'GB', 'United Kingdom', 'England', 'Bracknell', 51.41363, -0.75054, 'RG12 1HY', '+01:00'),
(289773312, 289773567, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(289773568, 289774335, 'GB', 'United Kingdom', 'England', 'Bracknell', 51.41363, -0.75054, 'RG12 1HY', '+01:00'),
(289774336, 289774591, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(289774592, 289777407, 'GB', 'United Kingdom', 'England', 'Bracknell', 51.41363, -0.75054, 'RG12 1HY', '+01:00'),
(289777408, 289777663, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(289777664, 289783807, 'GB', 'United Kingdom', 'England', 'Bracknell', 51.41363, -0.75054, 'RG12 1HY', '+01:00'),
(289783808, 289787903, 'GB', 'United Kingdom', 'England', 'London', 51.50853, -0.12574, 'WC2N 5RJ', '+01:00'),
(289787904, 289789951, 'DE', 'Germany', 'Berlin', 'Berlin', 52.52437, 13.41053, '10178', '+02:00'),
(289789952, 289791999, 'GB', 'United Kingdom', 'England', 'Bracknell', 51.41363, -0.75054, 'RG12 1HY', '+01:00'),
(289792000, 289793023, 'AE', 'United Arab Emirates', 'Dubayy', 'Dubai', 25.25817, 55.30472, '-', '+04:00'),
(289793024, 289793279, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(289793280, 289796095, 'GB', 'United Kingdom', 'England', 'Bracknell', 51.41363, -0.75054, 'RG12 1HY', '+01:00'),
(289796096, 289798143, 'TR', 'Turkey', 'Istanbul', 'Istanbul', 41.01384, 28.94966, '37770', '+03:00'),
(289798144, 289800191, 'SA', 'Saudi Arabia', 'Ar Riyad', 'Riyadh', 24.68773, 46.72185, '-', '+03:00'),
(289800192, 289931263, 'FR', 'France', 'Ile-de-France', 'Paris', 48.85341, 2.3488, '75000', '+01:00'),
(289931264, 289932031, 'IE', 'Ireland', 'Dublin', 'Dublin', 53.34399, -6.26719, 'D8', '+01:00'),
(289932032, 289932287, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(289932288, 289932543, 'IE', 'Ireland', 'Dublin', 'Dublin', 53.34399, -6.26719, 'D8', '+01:00'),
(289932544, 289933055, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(289933056, 289934079, 'IE', 'Ireland', 'Dublin', 'Dublin', 53.34399, -6.26719, 'D8', '+01:00'),
(289934080, 289934335, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(289934336, 289941759, 'IE', 'Ireland', 'Dublin', 'Dublin', 53.34399, -6.26719, 'D8', '+01:00'),
(289941760, 289942015, 'US', 'United States', 'California', 'Cupertino', 37.316603, -122.046483, '95014', '-07:00'),
(289942016, 289947647, 'IE', 'Ireland', 'Dublin', 'Dublin', 53.34399, -6.26719, 'D8', '+01:00'),
(289947648, 289949183, 'GB', 'United Kingdom', 'England', 'Bracknell', 51.41363, -0.75054, 'RG12 1HY', '+01:00'),
(289949184, 289949695, 'RU', 'Russian Federation', 'Moskva', 'Moscow', 55.75222, 37.61556, '101990', '+03:00'),
(289949696, 289950207, 'NL', 'Netherlands', 'Noord-Holland', 'Amsterdam', 52.37403, 4.88969, '1089', '+02:00'),
(289950208, 289950463, 'PL', 'Poland', 'Dolnoslaskie', 'Wroclaw', 51.1, 17.03333, '54-622', '+02:00'),
(289950464, 289950719, 'PT', 'Portugal', 'Lisboa', 'Lisbon', 38.71667, -9.13333, '4950-791', '+01:00'),
(289950720, 289951231, 'CZ', 'Czech Republic', 'Praha, Hlavni mesto', 'Prague', 50.08804, 14.42076, '150 00', '+02:00'),
(3758096384, 4294967295, '-', '-', '-', '-', 0, 0, '-', '-');

-- --------------------------------------------------------

--
-- Stand-in structure for view `subject_name_in_class`
--
CREATE TABLE `subject_name_in_class` (
`class_id` int(11)
,`sub_name` varchar(20)
,`sub_editor` varchar(20)
);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_books`
--

CREATE TABLE `tbl_books` (
  `id` int(11) NOT NULL,
  `book_name` varchar(255) NOT NULL,
  `book_price` varchar(255) NOT NULL,
  `book_author` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_books`
--

INSERT INTO `tbl_books` (`id`, `book_name`, `book_price`, `book_author`) VALUES
(51, 'book_name', 'book_price', 'book_author');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_class`
--

CREATE TABLE `tbl_class` (
  `class_id` int(11) NOT NULL,
  `class_name` varchar(22) NOT NULL,
  `class_level` varchar(22) NOT NULL,
  `sub_id` int(11) NOT NULL,
  `tech_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_class`
--

INSERT INTO `tbl_class` (`class_id`, `class_name`, `class_level`, `sub_id`, `tech_id`) VALUES
(0, 'Cheistry', 'XI', 3, 12),
(5, 'Mathematics', 'XII', 4, 14),
(8, 'Bio', '10 th', 2, 15),
(10, 'xxs', 'xsx', 0, 0),
(11, 'xxs', 'xsx', 0, 0),
(18, 'ccc', 'ccc', 0, 0),
(19, 'ccc', 'ccc', 0, 0),
(20, 'ccc', 'ccc', 0, 0),
(21, 'ccc', 'ccc', 0, 0),
(24, 'ccc', 'ccc', 0, 0),
(25, 'ccc', 'ccc', 0, 0),
(26, 'ccc', 'ccc', 0, 0),
(27, 'ccc', 'ccc', 0, 0),
(32, 'sufyan', 'afsar', 1, 12),
(33, 'sufyan', 'afsar', 1, 12),
(34, 'mathss', '5th', 4, 13),
(35, 'mathss', '5th', 4, 13);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_datepicker`
--

CREATE TABLE `tbl_datepicker` (
  `datepicker_id` int(11) NOT NULL,
  `datepicker_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_datepicker`
--

INSERT INTO `tbl_datepicker` (`datepicker_id`, `datepicker_date`) VALUES
(99, '2017-01-03'),
(106, '2017-01-26'),
(119, '2017-01-05'),
(120, '2017-06-08'),
(121, '2017-01-20'),
(122, '1970-01-01'),
(123, '2017-01-03'),
(124, '1970-01-01'),
(125, '2017-01-12'),
(126, '2017-01-18'),
(127, '2017-01-03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_person`
--

CREATE TABLE `tbl_person` (
  `personID` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `address` varchar(25) NOT NULL,
  `telephone` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_person`
--

INSERT INTO `tbl_person` (`personID`, `name`, `address`, `telephone`) VALUES
(1, 'sufyan', 'street 63 islalmabad', '123456789'),
(2, 'sajid', 'g 13 Islamabad', '987654321');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `std_id` int(11) NOT NULL,
  `std_parent` varchar(30) NOT NULL,
  `class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`std_id`, `std_parent`, `class_id`) VALUES
(1, 'nawaz', 2),
(2, 'khan', 2),
(3, 'khan', 3),
(4, 'neemat', 4),
(5, 'hussain', 5),
(6, 'khan', 3),
(7, 'khan', 3),
(8, 'neemat', 4),
(9, 'hussain', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_subject`
--

CREATE TABLE `tbl_student_subject` (
  `std_id` int(11) NOT NULL,
  `sub_id` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_subject`
--

INSERT INTO `tbl_student_subject` (`std_id`, `sub_id`, `id`) VALUES
(1, 1, 1),
(1, 2, 2),
(1, 1, 3),
(1, 2, 4),
(4, 4, 5),
(4, 3, 6),
(4, 2, 7),
(2, 2, 8),
(2, 1, 9),
(4, 4, 10),
(4, 3, 11),
(4, 2, 12),
(2, 2, 13),
(2, 1, 14);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_teacher`
--

CREATE TABLE `tbl_student_teacher` (
  `id` int(11) NOT NULL,
  `std_id` int(11) NOT NULL,
  `tech_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_teacher`
--

INSERT INTO `tbl_student_teacher` (`id`, `std_id`, `tech_id`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subject`
--

CREATE TABLE `tbl_subject` (
  `sub_id` int(11) NOT NULL,
  `sub_name` varchar(20) NOT NULL,
  `sub_editor` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subject`
--

INSERT INTO `tbl_subject` (`sub_id`, `sub_name`, `sub_editor`) VALUES
(1, 'Physics', 'Pnunjab book board'),
(2, 'Bio', 'Pnunjab book board'),
(3, 'Chemistry', 'Pnunjab book board'),
(4, 'Maths', 'Pnunjab book board');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teacher`
--

CREATE TABLE `tbl_teacher` (
  `tech_id` int(11) NOT NULL,
  `tech_name` varchar(20) NOT NULL,
  `tech_designation` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_teacher`
--

INSERT INTO `tbl_teacher` (`tech_id`, `tech_name`, `tech_designation`) VALUES
(2, 'Asif', 'teacher'),
(12, 'jillani', 'Teacher'),
(13, 'Sajid', 'Teacher'),
(14, 'sufyan', 'Teacher'),
(15, 'Farhan', 'Teacher'),
(16, 'Ali', 'teacher visitor');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tech_subject`
--

CREATE TABLE `tbl_tech_subject` (
  `id` int(11) NOT NULL,
  `tech_id` int(11) NOT NULL,
  `sub_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_test`
--

CREATE TABLE `tbl_test` (
  `test_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_lname` varchar(20) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_dob` date NOT NULL,
  `user_fname` varchar(20) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `date_format` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_lname`, `user_password`, `user_dob`, `user_fname`, `user_email`, `date_format`) VALUES
(162, 'dddd', 'b0baee9d279d34fa1dfd71aadb908c3f', '2017-01-03', 'dddd', 'schooladmin@educators-schools', 2),
(163, 'dddd', 'b0baee9d279d34fa1dfd71aadb908c3f', '2017-01-03', 'dddd', 'schooladmin@educators-schools', 2),
(164, 'dddd', 'b0baee9d279d34fa1dfd71aadb908c3f', '2017-01-03', 'dddd', 'schooladmin@educators-schools', 2),
(165, 'dddd', 'b0baee9d279d34fa1dfd71aadb908c3f', '2017-01-03', 'dddd', 'schooladmin@educators-schools', 2),
(168, 'sufyan', '3b712de48137572f3849aabd5666a4e3', '2017-01-03', 'afsar', 'sufyan@gmail.com', 1),
(169, 'ssss', '3b712de48137572f3849aabd5666a4e3', '2017-01-03', 'ssss', 'sufyan@gmail.com', 2),
(170, 'dew', 'b59c67bf196a4758191e42f76670ceba', '2017-01-03', 'edewd', 's@gmail.com', 1),
(171, 'admin', '1234', '2017-01-04', 'admin', 'm@gmail.com', 2),
(172, 'sss', 'b59c67bf196a4758191e42f76670ceba', '2017-01-30', 'sss', 'm@gmail.com', 0),
(173, 'ssss', '8f60c8102d29fcd525162d02eed4566b', '2017-02-09', 'ssss', 'ss@gmail', 0),
(174, 'sssss', 'b0baee9d279d34fa1dfd71aadb908c3f', '2017-02-27', 'sss', 'schooladmin@educators-schools', 0),
(175, 'schooladmin@educator', '6ce7e0f9cb84c0ccf426acb9dccc914e', '2017-03-07', 'schooladmin@educator', 'schooladmin@educators', 0),
(176, 'ssssssssssss', '25f9e794323b453885f5181f1b624d0b', '2017-04-25', 'ssssssssssssssss', 'maziz@gmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_login_history`
--

CREATE TABLE `tbl_user_login_history` (
  `id` int(11) NOT NULL,
  `tbl_user_id` int(11) NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `user_agent` varchar(100) NOT NULL,
  `user_os` varchar(100) NOT NULL,
  `login_time` datetime NOT NULL,
  `logout_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_login_history`
--

INSERT INTO `tbl_user_login_history` (`id`, `tbl_user_id`, `ip_address`, `user_agent`, `user_os`, `login_time`, `logout_time`) VALUES
(1, 176, '::1', 'Chrome - 57.6.2987.98', 'Windows 10', '2017-04-26 11:26:07', '2017-04-26 11:26:25'),
(2, 176, '::1', 'Chrome - 57.6.2987.98', 'Windows 10', '2017-04-26 11:26:46', '0000-00-00 00:00:00'),
(3, 175, '::1', 'Chrome - 57.6.2987.98', 'Windows 10', '2017-04-26 11:27:06', '0000-00-00 00:00:00'),
(4, 175, '::1', 'Chrome - 57.6.2987.98', 'Windows 10', '2017-04-26 11:27:21', '2017-04-26 11:27:30'),
(5, 176, '::1', 'Chrome - 57.6.2987.98', 'Windows 10', '2017-04-26 11:27:44', '2017-04-26 11:27:54'),
(6, 176, '::1', 'Chrome - 57.6.2987.98', 'Windows 10', '2017-04-26 11:28:04', '2017-04-26 12:47:48'),
(7, 176, '::1', 'Chrome - 57.6.2987.98', 'Windows 10', '2017-04-26 12:47:51', '0000-00-00 00:00:00'),
(8, 175, '::1', 'Opera - 43.0.2442.1144', 'Windows 10', '2017-04-26 13:06:37', '0000-00-00 00:00:00'),
(9, 175, '::1', 'Opera - 43.0.2442.1144', 'Windows 10', '2017-04-26 13:06:40', '0000-00-00 00:00:00'),
(10, 175, '::1', 'Opera - 43.0.2442.1144', 'Windows 10', '2017-04-26 13:10:57', '0000-00-00 00:00:00'),
(11, 175, '::1', 'Opera - 43.0.2442.1144', 'Windows 10', '2017-04-26 13:11:41', '0000-00-00 00:00:00'),
(12, 175, '::1', 'Opera - 43.0.2442.1144', 'Windows 10', '2017-04-26 13:11:42', '0000-00-00 00:00:00'),
(13, 176, '::1', 'Chrome - 57.6.2987.98', 'Windows 10', '2017-04-26 13:42:15', '0000-00-00 00:00:00'),
(14, 175, '::1', 'Opera - 43.0.2442.1144', 'Windows 10', '2017-04-26 14:28:26', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_login_history_old`
--

CREATE TABLE `tbl_user_login_history_old` (
  `id` int(11) NOT NULL,
  `Tbl_user_id` int(11) NOT NULL,
  `Ip_address` varchar(100) NOT NULL,
  `User_agent` varchar(100) NOT NULL,
  `User_os` varchar(100) NOT NULL,
  `Login_time` datetime NOT NULL,
  `Logout_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_login_history_old`
--

INSERT INTO `tbl_user_login_history_old` (`id`, `Tbl_user_id`, `Ip_address`, `User_agent`, `User_os`, `Login_time`, `Logout_time`) VALUES
(1, 176, '::1', 'Chrome - 57.6.2987.98', 'Windows 10', '2017-04-26 11:17:04', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `last_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `last_login`, `created_at`, `updated_at`, `email`) VALUES
(1, 'admin', '$1$Dtqyvz7/$wZSaZbfHgn0UbLlVi1HHp0', 'Admin', '2017-02-01 20:25:20', '2015-12-25 10:35:16', '2015-12-25 10:35:16', ''),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user', '2017-01-27 15:28:38', '2017-01-27 15:28:38', '2017-01-27 15:28:38', ''),
(3, 'sufyan', '12345678', '', '2017-01-30 11:05:30', '2017-01-30 11:05:30', '2017-01-30 11:05:30', '');

-- --------------------------------------------------------

--
-- Table structure for table `users_authentication`
--

CREATE TABLE `users_authentication` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `expired_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_authentication`
--

INSERT INTO `users_authentication` (`id`, `users_id`, `token`, `expired_at`, `created_at`, `updated_at`) VALUES
(1, 1, '$1$6fjNSBRR$7lx.mxo/q1LbNO7f5.7w8.', '2015-12-27 23:28:00', '2015-12-27 11:28:00', '2015-12-27 11:28:00'),
(2, 1, '$1$HY2H7rB0$2U.dlCsoHX21s/gvjCypG/', '2015-12-27 23:28:10', '2015-12-27 11:28:10', '2015-12-27 11:28:10'),
(4, 1, '$1$Mz5.lw4.$atw/6gp1rVQSIyhJ407ig.', '2017-01-28 04:47:56', '2017-01-27 14:47:56', '2017-01-27 14:47:56'),
(5, 1, '$1$hO/.Wr..$VyIBvA2RaYtCm3fsGZLjr0', '2017-01-28 04:53:14', '2017-01-27 14:53:14', '2017-01-27 14:53:14'),
(6, 1, '$1$kk1.d05.$VNz9cUPv3uUQnblwkmIwo/', '2017-01-28 04:53:46', '2017-01-27 14:53:46', '2017-01-27 14:53:46'),
(8, 1, '$1$Jd5.el0.$t49N1nCodum.brtwSZDgm.', '2017-01-28 05:04:37', '2017-01-27 15:04:37', '2017-01-27 15:04:37'),
(9, 1, '$1$s5/.FP4.$NET06Jbyp4zVDWGcwP8Zj/', '2017-01-28 05:04:59', '2017-01-27 15:04:59', '2017-01-27 15:04:59'),
(11, 1, '$1$G15.XK1.$FZFI.3pRxjKZpg0L2oyq30', '2017-01-28 05:28:05', '2017-01-27 15:28:05', '2017-01-27 15:28:05'),
(12, 1, '$1$U3/.NM3.$EK/h9DcAMAl0a2CHYnp7L/', '2017-01-28 05:30:00', '2017-01-27 15:30:00', '2017-01-27 15:30:00'),
(13, 1, '$1$nj4.k01.$6a3.drjX2YVGzMI2GF2yZ.', '2017-01-31 04:14:58', '2017-01-30 14:14:58', '2017-01-30 14:14:58'),
(14, 1, '$1$iu4.DF3.$HuAcBZRENv6yTEgfenP4X1', '2017-01-31 04:18:31', '2017-01-30 14:15:41', '2017-01-30 16:18:31'),
(15, 1, '$1$Z11.uS2.$aSls/prbboE4jODRYRBed0', '2017-01-31 04:15:53', '2017-01-30 14:15:53', '2017-01-30 14:15:53'),
(17, 1, '$1$3H2.Os5.$nMYXmx8J3FBf14inAsHwN/', '2017-02-01 01:47:21', '2017-01-31 11:47:21', '2017-01-31 11:47:21'),
(18, 1, '$1$Bw4.0v4.$zWJHwAcTy0LlrAm2nQMBY.', '2017-02-01 02:18:54', '2017-01-31 12:18:54', '2017-01-31 12:18:54'),
(19, 1, '$1$EU1.7w4.$q.4mrxtWd/otxqjCTdP510', '2017-02-01 02:19:12', '2017-01-31 12:19:12', '2017-01-31 12:19:12'),
(20, 1, '$1$bY/.IT2.$kDBCDoMaesOAiKMPWlFOJ.', '2017-02-02 03:20:11', '2017-02-01 13:20:11', '2017-02-01 13:20:11'),
(21, 1, '$1$mF/.110.$HY4KaAQsWnYlI75ZUKdg/0', '2017-02-02 05:13:46', '2017-02-01 15:12:29', '2017-02-01 17:13:46'),
(22, 1, '$1$vd/.M05.$qQf9kbmyaGrewOQaSXodA0', '2017-02-02 06:58:14', '2017-02-01 16:58:14', '2017-02-01 16:58:14'),
(23, 1, '$1$KH1.LB4.$mZk8eRUQbQscohrjYUaXD1', '2017-02-02 07:05:24', '2017-02-01 17:00:42', '2017-02-01 19:05:24'),
(25, 1, '$1$h3..W4..$MlB92CwxWTJj0Gv5HLkbU0', '2017-02-02 08:25:20', '2017-02-01 18:25:20', '2017-02-01 18:25:20');

-- --------------------------------------------------------

--
-- Structure for view `subject_name_in_class`
--
DROP TABLE IF EXISTS `subject_name_in_class`;

CREATE ALGORITHM=TEMPTABLE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `subject_name_in_class`  AS  select `tbl_class`.`class_id` AS `class_id`,`tbl_subject`.`sub_name` AS `sub_name`,`tbl_subject`.`sub_editor` AS `sub_editor` from (`tbl_subject` left join `tbl_class` on((`tbl_class`.`sub_id` = `tbl_subject`.`sub_id`))) order by (`tbl_class`.`class_id` = '5') ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `ip2location_db11`
--
ALTER TABLE `ip2location_db11`
  ADD KEY `idx_ip_from` (`ip_from`),
  ADD KEY `idx_ip_to` (`ip_to`),
  ADD KEY `idx_ip_from_to` (`ip_from`,`ip_to`);

--
-- Indexes for table `tbl_books`
--
ALTER TABLE `tbl_books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_class`
--
ALTER TABLE `tbl_class`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `tbl_datepicker`
--
ALTER TABLE `tbl_datepicker`
  ADD PRIMARY KEY (`datepicker_id`);

--
-- Indexes for table `tbl_person`
--
ALTER TABLE `tbl_person`
  ADD PRIMARY KEY (`personID`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`std_id`);

--
-- Indexes for table `tbl_student_subject`
--
ALTER TABLE `tbl_student_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student_teacher`
--
ALTER TABLE `tbl_student_teacher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_subject`
--
ALTER TABLE `tbl_subject`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `tbl_teacher`
--
ALTER TABLE `tbl_teacher`
  ADD PRIMARY KEY (`tech_id`);

--
-- Indexes for table `tbl_tech_subject`
--
ALTER TABLE `tbl_tech_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_user_login_history`
--
ALTER TABLE `tbl_user_login_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_login_history_old`
--
ALTER TABLE `tbl_user_login_history_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `users_authentication`
--
ALTER TABLE `users_authentication`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_books`
--
ALTER TABLE `tbl_books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `tbl_class`
--
ALTER TABLE `tbl_class`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tbl_datepicker`
--
ALTER TABLE `tbl_datepicker`
  MODIFY `datepicker_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `tbl_person`
--
ALTER TABLE `tbl_person`
  MODIFY `personID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
  MODIFY `std_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_student_subject`
--
ALTER TABLE `tbl_student_subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_student_teacher`
--
ALTER TABLE `tbl_student_teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_subject`
--
ALTER TABLE `tbl_subject`
  MODIFY `sub_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_teacher`
--
ALTER TABLE `tbl_teacher`
  MODIFY `tech_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_tech_subject`
--
ALTER TABLE `tbl_tech_subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;
--
-- AUTO_INCREMENT for table `tbl_user_login_history`
--
ALTER TABLE `tbl_user_login_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_user_login_history_old`
--
ALTER TABLE `tbl_user_login_history_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users_authentication`
--
ALTER TABLE `users_authentication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `dummy_entries` ON SCHEDULE EVERY 5 SECOND STARTS '2017-04-20 17:09:48' ON COMPLETION NOT PRESERVE ENABLE DO INSERT INTO tbl_test (test_name , ACTION)
VALUES (USER,  'inseerted byy events')$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
